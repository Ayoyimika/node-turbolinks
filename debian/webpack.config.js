var webpack = require("webpack");
var path = require('path');

module.exports = {
  output: {
    path: path.join(process.cwd(), './')
   },
  plugins: [
    new webpack.ProvidePlugin({
      'Turbolinks': './'
    })
  ]  

}
